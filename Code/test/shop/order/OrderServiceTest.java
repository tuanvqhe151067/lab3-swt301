/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop.order;

import org.hamcrest.collection.IsMapContaining;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import static org.hamcrest.MatcherAssert.assertThat;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import shop.product.Product;

/**
 *
 * @author Admin
 */
public class OrderServiceTest {

    private static OrderService orderService;
    private static LocalDate dateF;
    private static LocalDate dateT;
    //SonDT
    private static Order order;
    private static OrderItem oi1 = new OrderItem();
    private static OrderItem oi2 = new OrderItem();
    private static OrderItem oi3 = new OrderItem();
    private static List<OrderItem> listOrderItem;
    double result;
    //SonDT
    public OrderServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        //SonDT
        orderService = new OrderService();
        order = new Order();
        listOrderItem = new ArrayList<>();
        order.setId(1);
        order.setStatus(Order.STATUS_CANCELLED);
        //SonDT
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        orderService = new OrderService();
        //SonDT
        OrderServiceTest.oi1.setOrderId(1);
        OrderServiceTest.oi1.setProductId(1);
        OrderServiceTest.oi1.setPrice(200);
        OrderServiceTest.oi1.setQuantity(5);
        OrderServiceTest.listOrderItem.add(OrderServiceTest.oi1);
        OrderServiceTest.oi2.setOrderId(2);
        OrderServiceTest.oi2.setProductId(2);
        OrderServiceTest.oi2.setPrice(300);
        OrderServiceTest.oi2.setQuantity(5);
        OrderServiceTest.listOrderItem.add(OrderServiceTest.oi2);
        OrderServiceTest.oi3.setOrderId(3);
        OrderServiceTest.oi3.setProductId(3);
        OrderServiceTest.oi3.setPrice(400);
        OrderServiceTest.oi3.setQuantity(5);
        OrderServiceTest.listOrderItem.add(OrderServiceTest.oi3);
        //SonDT
    }
    
    @After
    public void tearDown() {
        orderService = null;
    }

    /**
     * Test of createOrder method, of class OrderService.
     */
    @Test
    public void testCreateOrder() {
        //Tesr case 1
        System.out.println("Result test case 1:");
        result = orderService.createOrder(order, listOrderItem);
        double exp = 4500.0;
        System.out.println(exp);
        System.out.println(result);
        assertEquals("Test case 1: ",new Double(exp), new Double(result));
        
        //Test case 2
        System.out.println("Result test case 2:");
        order.setId(2);
        order.setStatus(Order.STATUS_PENDING);
        result = orderService.createOrder(order, listOrderItem);
        double exp2 = 1000.0;
        System.out.println(exp2);
        System.out.println(result);
        assertEquals("Test case 2; ",new Double(exp2), new Double(result));
        
        //Test case 3
        System.out.println("Result test case 3:");
        order.setId(3);
        order.setStatus(Order.STATUS_PAID);
        result = orderService.createOrder(order, listOrderItem);
        double exp3 = 4500.0;
        System.out.println(exp3);
        System.out.println(result);
        assertEquals("Test case 3: ",new Double(exp3), new Double(result));
        
        //Test case 4
        System.out.println("Result test case 4:");
        order.setId(3);
        order.setStatus(Order.STATUS_CANCELLED);
        result = orderService.createOrder(order, listOrderItem);
        double exp4 = 4500.0;
        System.out.println(exp4);
        System.out.println(result);
        assertEquals("Test case 4: ",new Double(exp4), new Double(result));
    }

    /**
     * Test of calcCommission method, of class OrderService.
     */
    @Test
    public void testCalcCommission() {

        System.out.println("calcCommission");
        System.out.println("test case: 1");
        int salerId = 1;
        LocalDate dateF = LocalDate.of(2022, 5, 1);
        LocalDate dateT = LocalDate.of(2022, 5, 31);
        OrderService instance = new OrderService();
        double expResult = 0.0;
        double result = instance.calcCommission(salerId, dateF, dateT);
        assertEquals(expResult, result, 0.0);
        System.out.println("test case: 2");
        dateF = LocalDate.of(2022, 6, 1);
        dateT = LocalDate.of(2022, 6, 5);
        expResult = 250000.0;
        result = instance.calcCommission(salerId, dateF, dateT);
        assertEquals(expResult, result, 0.0);
        System.out.println("test case: 3");
        dateF = LocalDate.of(2022, 6, 1);
        dateT = LocalDate.of(2022, 6, 10);
        expResult = 2000000.0;
        result = instance.calcCommission(salerId, dateF, dateT);
        assertEquals(expResult, result, 0.0);
        System.out.println("test case: 4");
        dateF = LocalDate.of(2022, 6, 1);
        dateT = LocalDate.of(2022, 6, 30);
        expResult = 4750000.0;
        result = instance.calcCommission(salerId, dateF, dateT);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getSoldProducts method, of class OrderService.
     */
    
    @Test
    public void testGetSoldProductsTC1() {
        System.out.println("getSoldProducts-TC1");
        HashMap<Product, Double> expected = null;
        HashMap<Product, Double> actual = null;
        
        //TC1
        dateF = LocalDate.parse("2022-05-01");
        dateT = LocalDate.parse("2022-05-21");
        expected = new HashMap<>();
        actual = orderService.getSoldProducts(dateF, dateT);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetSoldProductsTC2() {
        System.out.println("getSoldProducts-TC2");
        HashMap<Product, Double> expected = null;
        HashMap<Product, Double> actual = null;
        
        //TC2
        dateF = LocalDate.parse("2023-06-01");
        dateT = LocalDate.parse("2023-06-21");
        expected = new HashMap<>();
        actual = orderService.getSoldProducts(dateF, dateT);
        assertEquals(expected, actual);
    }
    @Test
    public void testGetSoldProductsTC3() {
        System.out.println("getSoldProducts-TC3");
        HashMap<Product, Double> actual = null;

        //TC3
        dateF = LocalDate.parse("2022-04-01");
        dateT = LocalDate.parse("2022-04-21");
        actual = orderService.getSoldProducts(dateF, dateT);
        //kiểm tra xem trong map có bất kì giá trị của key nào bằng 100 không 
        assertThat(actual, IsMapContaining.hasValue(100));
    }
    @Test
    public void testGetSoldProductsTC4() {
        System.out.println("getSoldProducts-TC4");
        HashMap<Product, Double> actual = null;
        //TC4
        dateF = LocalDate.parse("2022-06-01");
        dateT = LocalDate.parse("2022-06-21");
        actual = orderService.getSoldProducts(dateF, dateT);
        //kiểm tra xem trong map có bất kì giá trị của key nào bằng 100 không 
        assertThat(actual, IsMapContaining.hasValue(100));
    }
    
    

    /**
     * Test of updateOrder method, of class OrderService.
     */
//    @Test
//    public void testUpdateOrder() {
//        System.out.println("updateOrder");
//        Order order = null;
//        List<OrderItem> items = null;
//        OrderService instance = new OrderService();
//        double expResult = 0.0;
//        double result = instance.updateOrder(order, items);
//        assertEquals(expResult, result, 0.0);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
}
