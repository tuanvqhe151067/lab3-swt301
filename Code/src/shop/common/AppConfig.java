/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop.common;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kiennt
 */
public class AppConfig {

    public static String DB_DRIVER = "test";
    public static String DB_HOST = "localhost";
    public static String DB_PORT = "3306";
    public static String DB_USER_NAME = "root";
    public static String DB_PASSWORD = "";
    public static String DB_NAME = "test";

    public static String LOG_FILE_PATH = "";
    public static Level LOG_LEVEL = Level.WARNING;
    
    public static Properties SETTING_TYPES = new Properties();
    
    public static Properties MAIL_PROPERTIES = new Properties();
    public static String MAIL_LOGIN = "kienntvn@gmail.com";
    public static String MAIL_PASSWORD = "abc123";
    
    public static String WELCOME_MESSAGE = "Initializing the application..";

    static {
        // must set before the Logger
        String filePath = AppConfig.class.getClassLoader().getResource("config/logger.config").getFile();
        System.setProperty("java.util.logging.config.file", filePath.replaceAll("%20", " "));
        Logger logger = Logger.getLogger("AppConfig");

        Properties props = new Properties();

        filePath = AppConfig.class.getClassLoader().getResource("config/app.config").getFile();
        try (FileInputStream fis = new FileInputStream(filePath.replaceAll("%20", " "))) {
            props.load(fis);
        } catch (IOException ex) {
            logger.log(Level.WARNING, "Error with initializing the app configs: {0}", ex.getMessage());
        }

        initDatabaseConfigs(props);
        initEmailConfigs(props);
        initConfigLists(props);

        logger.info("Initialized the app configs!");
        logger.config("Setting Types:" + SETTING_TYPES);
    }

    private static void initDatabaseConfigs(Properties prop) {
        DB_DRIVER = prop.getProperty("db.driver");
        DB_HOST = prop.getProperty("db.host");
        DB_PORT = prop.getProperty("db.port");
        DB_USER_NAME = prop.getProperty("db.user_name");
        DB_PASSWORD = prop.getProperty("db.password");
        DB_NAME = prop.getProperty("db.name");
    }
    
    private static void initEmailConfigs(Properties prop) {
        MAIL_LOGIN = prop.getProperty("mail.from");
        MAIL_PASSWORD = prop.getProperty("mail.password");
    }

    private static void initConfigLists(Properties props) {
        Enumeration<String> enums = (Enumeration<String>) props.propertyNames();
        while (enums.hasMoreElements()) {
            String key = enums.nextElement();
            String value = props.getProperty(key);
            
            if(key.startsWith("setting.type.")) {
                SETTING_TYPES.setProperty(key.replaceFirst("setting.type", ""), value);
            }
            
            if(key.startsWith("mail.smtp.")) {
                MAIL_PROPERTIES.setProperty(key, value);
            }
        }
    }
}
