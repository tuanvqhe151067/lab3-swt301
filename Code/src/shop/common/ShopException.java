/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shop.common;

/**
 *
 * @author macos
 */
public class ShopException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private final String sourceName;
    private final String fieldName;
    private final Object fieldValue;

    public ShopException(String sourceName, String fName, Object fValue, boolean realExc) {
        super(String.format(realExc ? "Exception %s occured with %s.%s: %s"
                : "%s not found with %s : '%s'", sourceName, fName, fValue));
        this.sourceName = sourceName;
        this.fieldName = fName;
        this.fieldValue = fValue;
    }

    public String getSourceName() {
        return sourceName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Object getFieldValue() {
        return fieldValue;
    }
    
}
