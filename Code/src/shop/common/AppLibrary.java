/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shop.common;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class AppLibrary {

    public static Connection getDbConn() throws SQLException, ShopException {
        Connection conn = null;
        try {
            Class.forName(AppConfig.DB_DRIVER);
            String connURL = "jdbc:mysql://" + AppConfig.DB_HOST + ":3306/" + AppConfig.DB_NAME;
            conn = DriverManager.getConnection(connURL, AppConfig.DB_USER_NAME, AppConfig.DB_PASSWORD);
        } catch (ClassNotFoundException exc) {
            throw new ShopException("ClassNotFoundException", "AppLibrary.getDbConn()", exc.getMessage(), true);
        } catch (SQLException exc) {
            throw new ShopException("SQLException", "AppLibrary.getDbConn()", exc.getMessage(), true);
        }

        return conn;
    }

    public static void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

    public static Date getSQLDate(LocalDate date) {
        return java.sql.Date.valueOf(date);
    }

    public static LocalDate getUtilDate(Date sqlDate) {
        return sqlDate.toLocalDate();
    }

    public static boolean sendMail(String fromEmail, String toEmail, String subject, String htmlContent) {
        try {
            Session session = Session.getDefaultInstance(AppConfig.MAIL_PROPERTIES, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(AppConfig.MAIL_LOGIN, AppConfig.MAIL_PASSWORD);
                }
            });
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(fromEmail));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
            message.setSubject(subject);
            message.setContent(htmlContent, "text/html");
            Transport.send(message);
            return true;
        } catch (MessagingException ex) {
            return false;
        }
    }
}
