package shop.order;

import java.util.List;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import shop.common.ShopException;
import shop.product.Product;
import shop.product.ProductRepository;

public class OrderService {

    // function to sort hashmap by values
    private HashMap<Product, Double> sortByValue(HashMap<Product, Double> hm) {
        // Create a list from elements of HashMap
        List<Map.Entry<Product, Double>> list = new LinkedList<>(hm.entrySet());

        // Sort the list using lambda expression
        Collections.sort(list, (i1, i2) -> i1.getValue().compareTo(i2.getValue()));

        // put data from sorted list to hashmap
        HashMap<Product, Double> temp = new LinkedHashMap<>();
        for (Map.Entry<Product, Double> aa : list) {
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;
    }

    private void checkProductAvails(List<OrderItem> items) throws ShopException {
        String productIds = "";
        String bookQuan = "";
        String availQuan = "";
        boolean validProducts = true;
        ProductRepository productRepo = new ProductRepository();
        HashMap<Integer, Product> products = productRepo.read(true);
        for (int i = 0; i < items.size(); i++) {
            OrderItem orderItem = items.get(i);
            Product product = products.get(orderItem.getProductId());
            if (product == null || product.getQuantity() < orderItem.getQuantity()) {
                validProducts = false;
                productIds = productIds + (productIds.length() > 0 ? ", " : "") + product.getId();
                bookQuan = bookQuan + (bookQuan.length() > 0 ? ", " : "") + orderItem.getQuantity();
                availQuan = availQuan + (availQuan.length() > 0 ? ", " : "") + product.getQuantity();
            }
        }
        if (!validProducts) {
            String exMsg = "ProductIds: " + productIds + " | Book: " + bookQuan + " | Avail: " + availQuan;
            throw new ShopException("UnavailableProduct", "OrderService.checkProductAvails()", exMsg, true);
        }
    }

    //This is to add new order & order items into the system database and return total order cost
    public double createOrder(Order order, List<OrderItem> items) throws ShopException {
        if (order == null || items == null || items.isEmpty()) {
            return -1;
        }
        
        if(order.getStatus() == Order.STATUS_PAID) {
            checkProductAvails(items);
        }

        double orderCost = 0;
        for (int i = 0; i < items.size(); i++) {
            OrderItem orderItem = items.get(i);
            if (orderItem == null) {
                continue;
            }
            orderCost += orderItem.getPrice() * orderItem.getQuantity();
        }

        order.setTotalCost(orderCost);
//        OrderRepository orderRepo = new OrderRepository();
//        orderRepo.createOrder(order, items);
        return orderCost;
    }

    //This is to calculate the commission of a specific saler for the time from fromDate to toDate
    public double calcCommission(int salerId, LocalDate dateF, LocalDate dateT) throws ShopException {
        double revenue = 0;
        double commission = 0;
        double level1 = 10000000;
        double level2 = level1 * 3;
        double rate1 = 0.05, rate2 = 0.1, rate3 = 0.15;

        OrderRepository orderRepo = new OrderRepository();
        List<Order> orders = orderRepo.read(salerId, Order.STATUS_PAID, dateF, dateT);
        for (int i = 0; i < orders.size(); i++) {
            Order order = orders.get(i);
            revenue += order.getTotalCost();
        }

        if (revenue <= 0) {
            return 0;
        }

        if (revenue < level1) {
            commission = revenue * rate1;
        } else if (revenue < level2) {
            commission = level1 * rate1 + (revenue - level1) * rate2;
        } else {
            commission = level1 * rate1 + (level2 - level1) * rate2 + (revenue - level2) * rate3;
        }

        return commission;
    }
    
    
    //Get the list of products which has been sold during from dateF to dateT
    //and relevant solved revenues, ordered by revenue
    public HashMap<Product, Double> getSoldProducts(LocalDate dateF, LocalDate dateT) throws ShopException {
        HashMap<Product, Double> soldProductRevs = new HashMap();
        OrderRepository orderRepo = new OrderRepository();
        //List<Product> soldProducts = orderRepo.read(Order.STATUS_PAID, dateF, dateT);
        List<Product> soldProducts = orderRepo.initSoldProducts(dateF, dateT);
        if (soldProducts == null || soldProducts.isEmpty()) {
            return soldProductRevs;
        }

        for (int i = 0; i < soldProducts.size(); i++) {
            Product product = soldProducts.get(i);
            Double soldRev = soldProductRevs.get(product);
            if (soldRev != null) {
                soldProductRevs.remove(product);
                soldRev += product.getPrice() * product.getQuantity();
            }

            soldProductRevs.put(product, soldRev);
        }

        return soldProductRevs;
    }

    //This is to add update order & order items into the system database and return total order cost
    public double updateOrder(Order order, List<OrderItem> items) throws ShopException {
        if (order == null || items == null || items.isEmpty()) {
            return -1;
        }
        
        if(order.getStatus() == Order.STATUS_PAID) {
            checkProductAvails(items);
        }

        double orderCost = 0;
        List<OrderItem>[] orderItems = new ArrayList[3];

        OrderRepository orderRepo = new OrderRepository();
        if (orderRepo.read(order.getId()) == null) {
            throw new ShopException("OrderService.updateOrder(): Order", "ID", order.getId(), false);
        }

        HashMap<Integer, OrderItem> deletedProducts = orderRepo.readItems(order.getId());

        for (int i = 0; i < items.size(); i++) {
            OrderItem orderItem = items.get(i);
            if (orderItem == null) {
                continue;
            }

            orderCost += orderItem.getPrice() * orderItem.getQuantity();
            if (deletedProducts.get(orderItem.getProductId()) != null) {
                orderItems[1].add(orderItem);
                deletedProducts.remove(orderItem.getProductId());
            } else {
                orderItems[0].add(orderItem);
            }
        }

        for (Integer productId : deletedProducts.keySet()) {
            orderItems[2].add(deletedProducts.get(productId));
        }

        order.setTotalCost(orderCost);
        orderRepo.update(order, orderItems);
        return orderCost;
    }
}
