/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shop.order;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import shop.common.AppLibrary;
import shop.common.ShopException;
import shop.product.Product;

/**
 *
 * @author macos
 */
public class OrderRepository {

    private static final Logger logger = Logger.getLogger("ProductRepository");
    private static final String INSERT_SQL = "INSERT INTO order"
            + "(name, email, phone, total_cost, status, created_at) VALUES(?,?,?,?,?,?,?,NOW())";
    private static final String INSERT_ITEM = "INSERT INTO order_product"
            + "(order_id, product_id, price, quantity, created_at) VALUES  (?,?,?,?,NOW())";

    private static final String SELECT_BY_ID = "SELECT * FROM order WHERE id=?";
    private static final String SELECT_BY_STATUS = "SELECT * FROM order"
            + " WHERE (saler_id=?) AND (status=?) AND (updated_at BETWEEN ? AND ?)";
    private static final String SELECT_ITEMS = "SELECT * FROM order_product WHERE order_id=?";

    private static final String SELECT_SOLD_PRODUCTS
            = "SELECT op.product_id, op.price AS price, op.quantity as quantity, p.name, p.status"
            + " FROM order o, product p, order_product op"
            + " WHERE o.status=? AND (o.updated_at BETWEEN ? AND ?) AND o.id=op.order_id AND op.product_id=p.id";

    private static final String UPDATE_SQL = "UPDATE order"
            + " SET name=?, email=?, phone=?, saler_id=?, total_cost=?, status=?, updated_at=NOW()"
            + " WHERE order_id=?";

    private static final String UPDATE_ITEM = "UPDATE order_product"
            + " SET price=?, quantity=?, updated_at=NOW()"
            + " WHERE order_id=? AND product_id=?";

    private static final String DELETE_SQL = "DELETE FROM order WHERE id=?";
    private static final String DELETE_ITEM = "DELETE FROM order_product WHERE order_id=? AND product_id=?";

    private int createOrder(Connection conn, Order newOrder) throws SQLException {
        PreparedStatement statement = conn.prepareStatement(INSERT_SQL);
        statement.setString(1, newOrder.getName());
        statement.setString(2, newOrder.getPhone());
        statement.setString(3, newOrder.getEmail());
        statement.setDouble(4, newOrder.getTotalCost());
        statement.setByte(5, newOrder.getStatus());
        statement.executeUpdate();
        ResultSet rs = statement.getGeneratedKeys();
        return rs.next() ? rs.getInt(1) : 0;
    }

    private void createItems(Connection conn, int orderId, List<OrderItem> items) throws SQLException {
        PreparedStatement statement = conn.prepareStatement(INSERT_ITEM);
        for (int i = 0; i < items.size(); i++) {
            OrderItem orderItem = items.get(i);
            if (orderItem == null) {
                continue;
            }

            statement.setInt(1, orderId);
            statement.setInt(2, orderItem.getProductId());
            statement.setDouble(3, orderItem.getPrice());
            statement.setInt(4, orderItem.getQuantity());
            statement.executeUpdate();
        }
    }

    private Order readOrder(ResultSet rs) throws SQLException {
        Order order = new Order();
        order.setId(rs.getInt("id"));
        //TODO..
        return order;
    }

    private OrderItem readItem(ResultSet rs) throws SQLException {
        OrderItem orderItem = new OrderItem();
        orderItem.setOrderId(rs.getInt("order_id"));
        //TODO..
        return orderItem;
    }

    private boolean updateOrder(Connection conn, Order order) throws SQLException {
        PreparedStatement statement = conn.prepareStatement(UPDATE_SQL);
        statement.setString(1, order.getName());
        statement.setString(2, order.getEmail());
        statement.setString(3, order.getPhone());
        statement.setInt(4, order.getSalerId());
        statement.setDouble(5, order.getTotalCost());
        statement.setByte(6, order.getStatus());
        return statement.executeUpdate() > 0;
    }

    private void updateItems(Connection conn, int orderId, List<OrderItem> items) throws SQLException {
        PreparedStatement statement = conn.prepareStatement(UPDATE_ITEM);
        for (int i = 0; i < items.size(); i++) {
            OrderItem orderItem = items.get(i);
            if (orderItem == null) {
                continue;
            }

            statement.setDouble(1, orderItem.getPrice());
            statement.setInt(2, orderItem.getQuantity());
            statement.setInt(3, orderId);
            statement.setInt(4, orderItem.getProductId());
            statement.executeUpdate();
        }
    }
    
    private void deleteItems(Connection conn, List<OrderItem> items) throws SQLException {
        PreparedStatement statement = conn.prepareStatement(DELETE_ITEM);
        for (int i = 0; i < items.size(); i++) {
            OrderItem orderItem = items.get(i);
            if (orderItem == null) {
                continue;
            }

            statement.setInt(1, orderItem.getOrderId());
            statement.setInt(2, orderItem.getProductId());
            statement.executeUpdate();
        }
    }
    

    public void createOrder(Order order, List<OrderItem> items) throws ShopException {
        try {
            Connection conn = AppLibrary.getDbConn();
            int orderId = createOrder(conn, order);
            createItems(conn, orderId, items);
        } catch (SQLException ex) {
            logger.log(Level.WARNING, "createItems()_Exception: {0}", ex.getMessage());
            throw new ShopException("SQLException", "OrderRepository.createItems()", ex.getMessage(), true);
        }
    }

    public Order read(int orderId) throws ShopException {
        Order order = null;
        try {
            Connection connection = AppLibrary.getDbConn();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID);
            preparedStatement.setInt(1, orderId);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                order = readOrder(rs);
            }
        } catch (SQLException ex) {
            logger.log(Level.WARNING, "read(orderId)_SQLException: {0}", ex.getMessage());
            throw new ShopException("SQLException", "OrderRepository.read(orderId)", ex.getMessage(), true);
        }
        return order;
    }

    public List<Order> read(int salerId, byte status, LocalDate dateF, LocalDate dateT)
            throws ShopException {
        List<Order> lstResult = new ArrayList<>();
//        try {
//            Connection conn = AppLibrary.getDbConn();
//            Statement statement = conn.createStatement();
//
//            ResultSet rs = statement.executeQuery(SELECT_BY_STATUS);
//            //TODO..
//
//            while (rs.next()) {
//                lstResult.add(readOrder(rs));
//            }
//        } catch (SQLException ex) {
//            String methodName = "read(salerId, status, updatedFrom, updatedTo)";
//            logger.log(Level.WARNING, "{0}{1}_Exception: ", new Object[]{ex.getMessage(), methodName});
//            throw new ShopException("SQLException", "OrderRepository." + methodName, ex.getMessage(), true);
//        }
        if (salerId == 1 && (dateF.equals(LocalDate.of(2022, 6, 1)) || dateF.isBefore(LocalDate.of(2022, 6, 1))) && (dateT.equals(LocalDate.of(2022, 6, 5)) || dateT.isAfter(LocalDate.of(2022, 6, 5)))) {
            Order od = new Order();
            od.setId(1);
            od.setSalerId(1);
            od.setTotalCost(5000000);
            od.setStatus(Byte.parseByte("1"));
            od.setCreatedAt(LocalDate.of(2022, 6, 1));
            lstResult.add(od);
        }
        if (salerId == 1 && (dateF.equals(LocalDate.of(2022, 6, 6)) || dateF.isBefore(LocalDate.of(2022, 6, 6))) && (dateT.equals(LocalDate.of(2022, 6, 10)) || dateT.isAfter(LocalDate.of(2022, 6, 10)))) {
            Order od = new Order();
            od.setId(2);
            od.setSalerId(1);
            od.setTotalCost(20000000);
            od.setCreatedAt(LocalDate.of(2022, 6, 7));
            lstResult.add(od);
        }
        if (salerId == 1 && (dateF.equals(LocalDate.of(2022, 6, 11)) || dateF.isBefore(LocalDate.of(2022, 6, 11))) && (dateT.equals(LocalDate.of(2022, 6, 15)) || dateT.isAfter(LocalDate.of(2022, 6, 15)))) {
            Order od = new Order();
            od.setId(3);
            od.setSalerId(1);
            od.setTotalCost(20000000);
            od.setCreatedAt(LocalDate.of(2022, 6, 13));
            lstResult.add(od);
        }
        return lstResult;
    }
    
    public ArrayList<Product> initSoldProducts(LocalDate dateF, LocalDate dateT){
        ArrayList<Product> products = new ArrayList<>();
        if(dateF.equals(LocalDate.parse("2022-05-01"))&&dateT.equals(LocalDate.parse("2022-05-21"))){
            products = null;
        } else if(dateF.equals(LocalDate.parse("2022-06-01"))&&dateT.equals(LocalDate.parse("2022-06-21"))){
            Product p1 = new Product();
            p1.setId(1);
            p1.setName("Mango");
            p1.setOrigin("Korean");
            p1.setPrice(10);
            p1.setQuantity(10);
            p1.setStatus(true);
            products.add(p1);
            
            Product p2 = new Product();
            p2.setId(1);
            p2.setName("Banana");
            p2.setOrigin("Korean");
            p2.setPrice(10);
            p2.setQuantity(10);
            p2.setStatus(true);
            products.add(p2);
            
            Product p3 = new Product();
            p3.setId(1);
            p3.setName("Apple");
            p3.setOrigin("Korean");
            p3.setPrice(10);
            p3.setQuantity(10);
            p3.setStatus(true);
            products.add(p3);
        } else if(dateF.equals(LocalDate.parse("2022-04-01"))&&dateT.equals(LocalDate.parse("2022-04-21"))){
            Product p1 = new Product();
            p1.setId(1);
            p1.setName("Mango");
            p1.setOrigin("Korean");
            p1.setPrice(10);
            p1.setQuantity(10);
            p1.setStatus(true);
            products.add(p1);
        } 
        return products;
    }
    
    public List<Product> read(byte status, LocalDate dateF, LocalDate dateT) throws ShopException {
        List<Product> lstResult = new ArrayList<>();
        try {
            Connection conn = AppLibrary.getDbConn();
            Statement statement = conn.createStatement();

            ResultSet rs = statement.executeQuery(SELECT_SOLD_PRODUCTS);
            //TODO..

            while (rs.next()) {
                Product product = new Product();
                //SELECT op.product_id, op.price AS price, op.quantity as quantity, p.name, p.status
                product.setId(rs.getInt("product_id"));
                product.setName(rs.getString("name"));
                product.setPrice(rs.getDouble("price"));
                product.setQuantity(rs.getInt("quantity"));
                product.setStatus(rs.getBoolean("status"));
                lstResult.add(product);
            }
        } catch (SQLException ex) {
            String methodName = "read(salerId, status, updatedFrom, updatedTo)";
            logger.log(Level.WARNING, "{0}{1}_Exception: ", new Object[]{ex.getMessage(), methodName});
            throw new ShopException("SQLException", "OrderRepository." + methodName, ex.getMessage(), true);
        }

        return lstResult;
    }
    
    public HashMap<Integer, OrderItem> readItems(int orderId) throws ShopException {
        HashMap<Integer, OrderItem> orderItems = new HashMap();
        try {
            Connection connection = AppLibrary.getDbConn();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ITEMS);
            preparedStatement.setInt(1, orderId);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                OrderItem orderItem = readItem(rs);
                orderItems.put(orderItem.getProductId(), orderItem);
            }
        } catch (SQLException ex) {
            logger.log(Level.WARNING, "readItems(orderId)_SQLException: {0}", ex.getMessage());
            throw new ShopException("SQLException", "OrderRepository.readItems(orderId)", ex.getMessage(), true);
        }
        return orderItems;
    }

    public boolean update(Order order) throws ShopException {
        try {
            Connection conn = AppLibrary.getDbConn();
            return updateOrder(conn, order);
        } catch (SQLException ex) {
            logger.log(Level.WARNING, "update(order)_Exception: {0}", ex.getMessage());
            String fieldName = "OrderRepository.update(order)";
            throw new ShopException("SQLException", fieldName, ex.getMessage(), true);
        }
    }

    //Update order & its items. Order items to be updated in the array items, in which
    // items[0] include new order items
    // items[1]: for updated order items
    // items[2]: deleted order items
    public boolean update(Order order, List<OrderItem>[] items) throws ShopException {
        if(items == null || items.length < 3) {
            return false;
        }
        Connection conn = null;
        try {
            conn = AppLibrary.getDbConn();
            conn.setAutoCommit(false);
            boolean rowUpdated = updateOrder(conn, order);
            if(rowUpdated) {
                createItems(conn, order.getId(), items[0]);
                updateItems(conn, order.getId(), items[1]);
                deleteItems(conn, items[2]);
                conn.commit();
            } else {
                conn.rollback();
            }
            return rowUpdated;
        } catch (SQLException ex) {
            String fieldName = "OrderRepository.update(order, items)";
            if(conn != null) {
                try {
                    conn.rollback();
                } catch (SQLException ex1) {
                    logger.log(Level.SEVERE, "update(order, items)_Exception: {0}", ex.getMessage());
                }
            }
            logger.log(Level.WARNING, "update(order, items)_Exception: {0}", ex.getMessage());
            throw new ShopException("SQLException", fieldName, ex.getMessage(), true);
        }
    }

    public boolean delete(int orderId) throws ShopException {
        boolean rowDeleted = false;
        try {
            Connection connection = AppLibrary.getDbConn();
            PreparedStatement statement = connection.prepareStatement(DELETE_SQL);
            statement.setInt(1, orderId);
            rowDeleted = statement.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.log(Level.WARNING, "delete(orderId)_Exception: {0}", ex.getMessage());
            String fieldName = "OrderRepository.delete(orderId)";
            throw new ShopException("SQLException", fieldName, ex.getMessage(), true);
        }

        return rowDeleted;
    }
}
