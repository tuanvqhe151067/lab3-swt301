/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package shop.product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import shop.common.AppLibrary;
import shop.common.ShopException;

public class ProductRepository {

    private static final Logger logger = Logger.getLogger("ProductRepository");
    private static final String INSERT_SQL
            = "INSERT INTO product(name, origin, price, quantity, status) VALUES " + " (?,?,?,?,?)";
    
    private static final String SELECT_ALL = "SELECT * FROM product";
    private static final String SELECT_BY_ID = "SELECT * FROM product WHERE id=?";
    private static final String SELECT_BY_STATUS = "SELECT * FROM product WHERE status=?";
    
    private static final String UPDATE_SQL
            = "UPDATE product SET name=?, origin=?, price=?, quantity=?, status=? WHERE id=?";
    
    private static final String DELETE_SQL = "DELETE FROM product WHERE id=?";

    private Product readData(ResultSet rs) throws SQLException {
        Product product = new Product();
        product.setId(rs.getInt("id"));
        product.setName(rs.getString("name"));
        product.setOrigin(rs.getString("origin"));
        product.setPrice(rs.getDouble("price"));
        product.setQuantity(rs.getInt("quantity"));
        product.setStatus(rs.getBoolean("status"));

        return product;
    }

    public void create(Product newProduct) throws ShopException {
        try {
            Connection conn = AppLibrary.getDbConn();
            PreparedStatement statement = conn.prepareStatement(INSERT_SQL);
            statement.setString(1, newProduct.getName());
            statement.setString(2, newProduct.getOrigin());
            statement.setDouble(3, newProduct.getPrice());
            statement.setInt(4, newProduct.getQuantity());
            statement.setBoolean(5, newProduct.getStatus());
            statement.executeUpdate();
        } catch (SQLException ex) {
            logger.log(Level.WARNING, "create()_Exception: {0}", ex.getMessage());
            throw new ShopException("SQLException", "ProductRepository.create()", ex.getMessage(), true);
        }
    }

    public Product read(int productId) throws ShopException {
        Product product = null;
        try {
            Connection connection = AppLibrary.getDbConn();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID);
            preparedStatement.setInt(1, productId);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                product = readData(rs);
            }
        } catch (SQLException ex) {
            logger.log(Level.WARNING, "read(productId)_Exception: {0}", ex.getMessage());
            throw new ShopException("SQLException", "ProductRepository.read(productId)", ex.getMessage(), true);
        }
        return product;
    }

    public List<Product> read() throws ShopException {
        List<Product> lstResult = new ArrayList<>();
        try ( Connection conn = AppLibrary.getDbConn()) {
            Statement statement = conn.createStatement();

            ResultSet rs = statement.executeQuery(SELECT_ALL);

            while (rs.next()) {
                lstResult.add(readData(rs));
            }
        } catch (SQLException ex) {
            logger.log(Level.WARNING, "read()_Exception: {0}", ex.getMessage());
            throw new ShopException("SQLException", "ProductRepository.read()", ex.getMessage(), true);
        }

        return lstResult;
    }
    
    public HashMap<Integer, Product> read(boolean status) throws ShopException {
        HashMap<Integer, Product> result = new HashMap();
        try ( Connection conn = AppLibrary.getDbConn()) {
            PreparedStatement statement = conn.prepareStatement(SELECT_BY_STATUS);
            statement.setBoolean(1, status);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                Product product = readData(rs);
                result.put(product.getId(), product);
            }
        } catch (SQLException ex) {
            logger.log(Level.WARNING, "read()_Exception: {0}", ex.getMessage());
            throw new ShopException("SQLException", "ProductRepository.read()", ex.getMessage(), true);
        }

        return result;
    }

    public boolean update(Product product) throws ShopException {
        boolean rowUpdated = false;
        try {
            Connection connection = AppLibrary.getDbConn();
            PreparedStatement statement = connection.prepareStatement(UPDATE_SQL);
            statement.setString(1, product.getName());
            statement.setString(2, product.getOrigin());
            statement.setDouble(3, product.getPrice());
            statement.setInt(3, product.getQuantity());
            statement.setBoolean(5, product.getStatus());
            statement.setInt(6, product.getId());
            rowUpdated = statement.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.log(Level.WARNING, "update()_Exception: {0}", ex.getMessage());
            throw new ShopException("SQLException", "ProductRepository.update()", ex.getMessage(), true);
        }
        return rowUpdated;
    }

    public boolean delete(int id) throws ShopException {
        boolean rowDeleted = false;
        try {
            Connection connection = AppLibrary.getDbConn();
            PreparedStatement statement = connection.prepareStatement(DELETE_SQL);
            statement.setInt(1, id);
            rowDeleted = statement.executeUpdate() > 0;
        } catch (SQLException ex) {
            logger.log(Level.WARNING, "delete()_Exception: {0}", ex.getMessage());
            throw new ShopException("SQLException", "ProductRepository.delete()", ex.getMessage(), true);
        }

        return rowDeleted;
    }

}
